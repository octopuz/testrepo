﻿using NUnit.Framework;
using Test;

namespace Tests
{
    [TestFixture]
    public class CalculatorTest
    {
        [Test]
        public void SumTest()
        {
            Calculator calc = new Calculator();

            var result = calc.Sum(12, 12);

            Assert.IsTrue(result==24);
        }

        [Test]
        public void SumTestFailed()
        {
            Calculator calc = new Calculator();

            var result = calc.Sum(12, 12);

            Assert.IsTrue(result == 24);
        }
    }
}
